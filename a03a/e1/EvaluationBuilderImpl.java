package a03a.e1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import a03a.e1.Evaluation.Question;
import a03a.e1.Evaluation.Result;



public class EvaluationBuilderImpl implements EvaluationBuilder{
	
	private boolean builtAlready = false;
	private final Map<Pair<String, Integer>, Map<Question, Result>> evalMap = new HashMap<>();
	@Override
	public EvaluationBuilder addEvaluationByMap(String course, int student, Map<Question, Result> results) {
		evalMap.put(new Pair<String, Integer>(course, student), results);
		return this;
	}

	@Override
	public EvaluationBuilder addEvaluationByResults(String course, int student, Result resOverall, Result resInterest,
			Result resClarity) {
		Map<Question, Result> map = new HashMap<>();
		map.put( Question.OVERALL, resOverall);
		map.put(Question.INTEREST, resInterest);
		map.put(Question.CLARITY, resClarity);
		return addEvaluationByMap(course, student, map);
	}

	@Override
	public Evaluation build() {
		if(builtAlready) {
			throw new IllegalStateException("Already built");
		}
	builtAlready = true;
		return new Evaluation() {

			@Override
			public Map<Question, Result> results(String course, int student) {
				return evalMap.getOrDefault(new Pair<String, Integer>(course, student), new HashMap<>());
			}

			@Override
			public Map<Result, Long> resultsCountForCourseAndQuestion(String course, Question questions) {
				Map<Result, Long> map = new HashMap<>();
				map.put(Result.FULLY_NEGATIVE, Integer.toUnsignedLong(0));
				map.put(Result.FULLY_POSITIVE, Integer.toUnsignedLong(0));
				map.put(Result.WEAKLY_NEGATIVE, Integer.toUnsignedLong(0));
				map.put(Result.WEAKLY_POSITIVE, Integer.toUnsignedLong(0));
				
				for(Pair<String, Integer> pair: evalMap.keySet()) {
					if(pair.getX().equals(course)) {
						for(Map.Entry<Question, Result> entry: evalMap.get(pair).entrySet()) {
							if(entry.getKey().equals(questions)) {
								map.put(entry.getValue(), map.get(entry.getValue())+1);
							}
						}
					}
				}
				return map;
			}

			@Override
			public Map<Result, Long> resultsCountForStudent(int student) {
				Map<Result, Long> map = new HashMap<>();
				for(Pair<String, Integer> pair: evalMap.keySet()) {
					if(pair.getY().equals(student)) {
						for(Map.Entry<Question, Result> entry: evalMap.get(pair).entrySet()) {
							map.put(entry.getValue(), (map.getOrDefault(entry.getValue(), Integer.toUnsignedLong(0))+1));
						}
					}
				}
				return map;
			}

			@Override
			public double coursePositiveResultsRatio(String course, Question question) {
				// TODO Auto-generated method stub
				return 0;
			}
			
		};
	}
	
}
