package a03a.e2;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.swing.*;

public class GUI extends JFrame {
    
    
	private static final long serialVersionUID = -6218820567019985015L;
	private final Map<JButton, Integer> valuesMap = new HashMap<>();
    private final Map<JButton, Pair<Integer, Integer>> buttonsMap = new HashMap<>();
    private static final int SIZE = 6;
    
    public GUI() {
    	
    	this.setSize(200,200);
        
        JPanel panel = new JPanel(new GridLayout(SIZE, SIZE));
        this.getContentPane().add(BorderLayout.CENTER,panel);
        
        ActionListener al = (e)->{
            final JButton bt = (JButton)e.getSource();
            isHit(bt);
            shouldDisable(bt);
            isOver(buttonsMap);
            
        };
                
        for (int i=0; i<SIZE; i++){
            for (int j=0; j<SIZE; j++){
                final JButton jb = new JButton("");
                jb.addActionListener(al);
                valuesMap.put(jb, 0);
                this.buttonsMap.put(jb, new Pair<Integer, Integer>(i, j));
                panel.add(jb);
            }
        }
        this.setVisible(true);
    }
        
    private void isOver(Map<JButton, Pair<Integer, Integer>> buttonsMap) {
    	int counter = 0;
		for(JButton button: buttonsMap.keySet()) {
			if(valuesMap.get(button) >= 1) {
				counter++;
			}
		}
		if(counter == SIZE*SIZE) {
			System.exit(0);
		}
	}	

	private void shouldDisable(JButton bt) {
		Pair<Integer, Integer> pos = buttonsMap.get(bt);
		for(JButton button : buttonsMap.keySet()) {
			Pair<Integer, Integer> pos2 = buttonsMap.get(button);
			if(Math.abs(pos.getX()-pos2.getX())<=1 && Math.abs(pos.getY()-pos2.getY())<=1) {
				if(Integer.parseInt(button.getText()) >= 5){
					button.setEnabled(false);
				}
			}
		}
	}

	private void isHit(JButton bt) {
		Pair<Integer, Integer> pos = buttonsMap.get(bt);
		for(JButton button : buttonsMap.keySet()) {
			Pair<Integer, Integer> pos2 = buttonsMap.get(button);
			if(Math.abs(pos.getX()-pos2.getX())<=1 && Math.abs(pos.getY()-pos2.getY())<=1) {
				valuesMap.put(button, valuesMap.get(button)+1);
				button.setText(Integer.toString(valuesMap.get(button)));
			}
		}	
	}

	public static void main(String[] args) {
    	new GUI();
    }
}
