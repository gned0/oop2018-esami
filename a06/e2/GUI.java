package a06.e2;

import java.awt.*;
import java.awt.event.ActionListener;
import java.util.stream.*;
import javax.swing.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class GUI extends JFrame{
	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2173085645506952756L;

	private HashMap<Integer, JButton> buttonMap = new HashMap<Integer, JButton>();
	
	public GUI(int size){
		this.setSize(500, 100);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.getContentPane().setLayout(new FlowLayout());
		
		for(int i = 0; i < size; i++) {
			JButton jb = new JButton("1");
			buttonMap.put(i, jb);
		}
		
		
		
		JButton resetButton = new JButton("Reset");
		
		
		ActionListener ac = e -> {
			final JButton jb = (JButton)e.getSource();
			jb.setText(sum(jb).toString());
			jb.setEnabled(false);
		};
		
		
		ActionListener reset = e -> {
			final JButton bb = (JButton)e.getSource();
			for(JButton button: buttonMap.values()) {
				button.setEnabled(true);
				button.setText("1");
			}
			
		};
		
		for(JButton jb : buttonMap.values()) {
			jb.addActionListener(ac);
		}
		
		resetButton.addActionListener(reset);
		
		for(JButton jb : buttonMap.values()) {
			this.getContentPane().add(jb);
		}
		
		this.getContentPane().add(resetButton);

		
		this.setVisible(true);
		
		
	}
	
	

	private Integer sum(JButton jb) {
		//HashMap<JButton, Integer> sumMap = new HashMap<JButton, Integer>();
		JButton hitButton = new JButton();
		int sumLimit = 0;
		int sum = 0;
		for(Map.Entry<Integer, JButton> entry : buttonMap.entrySet()) {
			if(entry.getValue().equals(jb)) {
				hitButton = entry.getValue();
				sumLimit = entry.getKey();
			}
		}
		System.out.println("limit = " + sumLimit);
		for(Integer a : buttonMap.keySet()) {
			if(a <= sumLimit) {
				sum = sum + Integer.parseInt(buttonMap.get(a).getText());
			}
		}
	
		return sum;
		
	}



	public static void main(String[] args){
		new GUI(5);
	}
	
	

}
