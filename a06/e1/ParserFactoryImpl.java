package a06.e1;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class ParserFactoryImpl implements ParserFactory {

	@Override
	public Parser one(String token) {
		return this.oneOf(Collections.singleton(token));
	}

	@Override
	public Parser many(String token, int elemCount) {
		return new Parser() {
			private boolean consumed = false;
			private int counter = elemCount;
			private final String firstOccurence = token;
			@Override
			public boolean acceptToken(String token) {
				if(inputCompleted()) {
					return false;
				}
				consumed = true;
				counter--;
				return token.equals(firstOccurence);
			}

			@Override
			public boolean inputCompleted() {
				return counter <= 0;
			}

			@Override
			public void reset() {
				counter = elemCount;
				consumed = true;
			}
			
		};
	}

	@Override
	public Parser oneOf(Set<String> set) {
		return new Parser() {

			private boolean consumed = false;
			@Override
			public boolean acceptToken(String token) {
				if(inputCompleted()) {
					return false;
				}
				consumed = true;
				return set.contains(token);
			}

			@Override
			public boolean inputCompleted() {
				return consumed;
			}

			@Override
			public void reset() {
				consumed = false;
			}
			
		};
	}

	@Override
	public Parser sequence(String token1, String token2) {
		return new Parser() {

			private boolean consumedFirst = false;
			private boolean consumedSecond = false;
			private String first = token1;
			private String second = token2;
			@Override
			public boolean acceptToken(String token) {
				if(inputCompleted()) {
					return false;
				}
				if(!consumedFirst && token.equals(first)) {
					consumedFirst = true;
					return true;
				}
				if(!consumedSecond && token.equals(second)) {
					consumedSecond = true;
					return true;
				}
				return false;
			}

			@Override
			public boolean inputCompleted() {
				return consumedFirst && consumedSecond;
			}

			@Override
			public void reset() {
				consumedFirst = false;
				consumedSecond = false;
			}
			
		};
	}

	@Override
	public Parser fullSequence(String begin, Set<String> elem, String separator, String end, int elemCount) {
		// TODO Auto-generated method stub
		return null;
	}

}
