package a02b.e2;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;

import javax.swing.*;

public class GUI extends JFrame {
    
	private static final long serialVersionUID = -6218820567019985015L;
    
    public GUI() {
    	
    	
        this.setSize(500,500);
        LogicsImpl logics = new LogicsImpl();;
        JPanel panel = new JPanel(new GridLayout(10,10));
        this.getContentPane().add(BorderLayout.CENTER,panel);
        
        ActionListener al = (e)->{
            final JButton bt = (JButton)e.getSource();
            logics.hit(bt);
            logics.createRect();
      
        };     
                
        for (int i=0; i<10; i++){
            for (int j=0; j<10; j++){
                final JButton jb = new JButton(" ");
                jb.addActionListener(al);
                logics.addButtons(jb, new Pair<Integer, Integer>(i, j));
                panel.add(jb);
            }
        }
        this.setVisible(true);
    }

	
}