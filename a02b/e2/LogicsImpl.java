package a02b.e2;

import java.util.HashMap;
import java.util.Map;

import javax.swing.JButton;

public class LogicsImpl {

	private boolean firstPresent = false;
	private boolean secondPresent = false;
	private JButton firstButton = new JButton();
	private JButton secondButton = new JButton();
	private Map<JButton, Pair<Integer, Integer>> buttonsMap = new HashMap<>();

	
	public void createRect() {
		int buttonsInRect = 0;
		int buttonsSet = 0;
		if(firstPresent && secondPresent) {	
			for(JButton button : buttonsMap.keySet()) {
				if(isInsideRect(button)) {
					buttonsInRect++;
					if(!button.getText().equals("*")) {
						button.setText("*");
					} else {
						buttonsSet++;
					}
				}
			}		
			firstPresent = false;
			secondPresent = false;
			firstButton.setEnabled(true);
			secondButton.setEnabled(true);
			this.isOver(buttonsInRect, buttonsSet);
		}
	}

	private boolean isInsideRect(JButton button) {
		Pair<Integer, Integer> posFirst = buttonsMap.get(firstButton);
		Pair<Integer, Integer> posSecond = buttonsMap.get(secondButton);
		int lengthX = Math.abs(posFirst.getX()-posSecond.getX());
		int lengthY = Math.abs(posFirst.getY()-posSecond.getY());
		Pair<Integer, Integer> posButton = buttonsMap.get(button);
		if(Math.abs(posButton.getX()-posFirst.getX())<= lengthX && Math.abs(posButton.getX()-posSecond.getX())<= lengthX) {
			if(Math.abs(posButton.getY()-posFirst.getY())<= lengthY && Math.abs(posButton.getY()-posSecond.getY())<= lengthY) {
				return true;
			}
		}
		return false;
	}

	private void isOver(int buttonsInRect, int buttonsSet) {
		if(buttonsInRect == buttonsSet) {
			System.out.println("GAME OVER");
			System.exit(0);
		}
	}

	public void hit(JButton bt) {
		bt.setEnabled(false);
		bt.setText("*");
		if(!firstPresent) {
			firstPresent = true;
			firstButton = bt;
		} else {
			secondPresent = true;
			secondButton = bt;
		}
		
	}

	public void addButtons(JButton jb, Pair<Integer, Integer> pair) {
		buttonsMap.put(jb, pair);
	}
}
