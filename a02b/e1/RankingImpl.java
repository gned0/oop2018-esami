package a02b.e1;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class RankingImpl implements Ranking {
	
	private List<Pair<String, Integer>> points = new ArrayList<>();
	private List<Tournament> tournaments = new LinkedList<>();

	Tournament tournament;
	@Override
	public void loadTournament(Tournament tournament) {
		tournaments.add(0, tournament);
	}
	
	private boolean inCurrentYear(Tournament t) {
		
		return t.getYear()==this.tournaments.get(0).getYear() || (
				t.getYear()==this.tournaments.get(0).getYear()-1 &&
				t.getWeek()>this.tournaments.get(0).getWeek());
	}		

	@Override
	public int getCurrentWeek() {
		return this.tournaments.get(0).getWeek();
	}

	@Override
	public int getCurrentYear() {
		return this.tournaments.get(0).getYear();
	}

	@Override
	public Integer pointsFromPlayer(String player) {
		
		return tournaments.stream()
				   .filter(t -> inCurrentYear(t))
				   .map(t -> t.getResult(player))
				   .filter(Optional::isPresent)
				   .map(Optional::get)
				   .reduce(0, (x,y)->x+y);
		}

	@Override
	public List<String> ranking() {
		


		return this.tournaments.stream()
				               .flatMap(t->t.getPlayers().stream())
				               .distinct()
				               .map(p -> new Pair<>(p,pointsFromPlayer(p)))
				               .sorted((p1,p2)->p2.getY()-p1.getY())
				               .map(Pair::getX)
				               .collect(Collectors.toList());
	}

	@Override
	public Map<String, String> winnersFromTournamentInLastYear() {
		Map<String, String> map = new HashMap<>();
		for(Tournament t : tournaments) {
			if(inCurrentYear(t)) {
				map.put(t.getName(), t.winner());
			}
		}
		return map;
	}


	@Override
	public Map<String, Integer> pointsAtEachTournamentFromPlayer(String player) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Pair<String, Integer>> pointsAtEachTournamentFromPlayerSorted(String player) {
		// TODO Auto-generated method stub
		return null;
	}

}
