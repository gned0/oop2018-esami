package a02b.e1;

import java.util.Map;
import java.util.Optional;
import java.util.Set;

public class TournamentFactoryImpl implements TournamentFactory {

	@Override
	public Tournament make(String name, int year, int week, Set<String> players, Map<String, Integer> points) {
		return new Tournament() {

			@Override
			public String getName() {
				return name;
			}

			@Override
			public int getYear() {
				return year;
			}

			@Override
			public int getWeek() {
				return week;
			}

			@Override
			public Set<String> getPlayers() {
				return players;
			}
			
			@Override
			public Optional<Integer> getResult(String player) {
				if(players.contains(player)) {
					if(points.containsKey(player)) {
						return Optional.of(points.get(player));
					}
					else {
						return Optional.of(0);
					}
				} 
				return Optional.empty();
			}

			@Override
			public String winner() {
				int max = 0;
				String winner = "empty";
				for (Map.Entry<String, Integer> entry : points.entrySet()) {
					if(entry.getValue() > max) {
						max = entry.getValue();
						winner = entry.getKey();
					}
				}
				return winner;
			}
			
		};
	}

}
