package a04.e2;

import javax.swing.*;
import java.util.*;
import java.util.List;
import java.awt.*;
import java.awt.event.ActionListener;

public class GUI extends JFrame {
    
	private Boolean isFirstOne = true;
	private HashMap<JButton, Pair<Integer, Integer>> buttonMap = new HashMap<>();
	
	private static final long serialVersionUID = -6218820567019985015L;
    private final static int SIZE = 4;
    private final List<JButton> jbs = new ArrayList<>();
    
    public GUI() {
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(100*SIZE, 100*SIZE);
        
        JPanel panel = new JPanel(new GridLayout(SIZE,SIZE));
        this.getContentPane().add(BorderLayout.CENTER,panel);
        
        ActionListener al = (e)->{
            final JButton bt = (JButton)e.getSource();
            this.hit(bt);
            this.isOver(bt);
        };
                
        for (int i=0; i<SIZE; i++){
            for (int j=0; j<SIZE; j++){
                final JButton jb = new JButton(" ");
                jb.addActionListener(al);
                buttonMap.put(jb, new Pair<Integer, Integer>(i, j));
                panel.add(jb);
            }
        }
        this.setVisible(true);
    }
    private void isOver(JButton bt) {
		 int disabledButtons = 0;
		 int adjButtons = 0;
  	for(JButton button : buttonMap.keySet()) {
  		if(!button.isEnabled()) {
  			disabledButtons++;
  		}
  		if(areAdj(bt, button) && button.isEnabled()) {
  			adjButtons++;
  		}
  	}
  	System.out.println("ADJ = " + adjButtons);
  	if(disabledButtons == SIZE*SIZE) {
  		 System.exit(1);
  	}
  	if(adjButtons <= 0) {
  		System.exit(1);
  	}
	}
	private void hit(JButton bt) {    	
    	if(isFirstOne) {
    		bt.setEnabled(false);
    		isFirstOne = false;
    	} else {
    		if(hasAdjacentHit(bt)) {	
    			bt.setEnabled(false);
    		}
    	}
    	
    	
	}
	private boolean hasAdjacentHit(JButton bt) {
		for(JButton button : buttonMap.keySet()) {
			if( (button.getX()  == bt.getX() && (button.getY() - 90 == bt.getY() || button.getY() + 90 == bt.getY()))) {
				if(!button.isEnabled()) {
					return true;
				}
			}
			if( button.getX() - 96 == bt.getX() && (button.getY() - 90 == bt.getY() || button.getY() == bt.getY() || button.getY() + 990 == bt.getY())) {
				if(!button.isEnabled()) {
					return true;
				}
			}
			if( button.getX() + 96 == bt.getX() && (button.getY() - 90 == bt.getY() || button.getY() == bt.getY() || button.getY() + 96 == bt.getY())) {
				if(!button.isEnabled()) {
					return true;
				}
			}
		}
		return false;
	}
	
	private boolean areAdj(JButton bt, JButton button) {
		System.out.println("bt = " + bt.getX() + ", " + bt.getY() + ", latest =" + button.getX() + " , "  + button.getY());
		if((button.getX() - 96) == bt.getX() && button.getY() == bt.getY() ) {
			return true;
		}
		if((button.getX() + 96) == bt.getX() && button.getY() == bt.getY()) {
			return true;
		}
		if((button.getY() - 90) == bt.getY() && button.getX() == bt.getX()) {
			return true;
		}
		if((button.getY() + 90) == bt.getY() && button.getX() == bt.getX()) {
			return true;
		}
		return false;
	}
	public static void main(String[] args) {
    	new GUI();
    }
}
