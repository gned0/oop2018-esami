package a04.e1;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

public class IntegerIteratorsFactoryImpl implements IntegerIteratorsFactory {

	@Override
	public SimpleIterator<Integer> empty() {
		return new SimpleIterator<Integer>() {

			@Override
			public Optional<Integer> next() {
				return Optional.empty();
			}
			
		};
	}

	@Override
	public SimpleIterator<Integer> fromList(List<Integer> list) {
		return new SimpleIterator<Integer>() {
			private int counter = 0;
			@Override
			public Optional<Integer> next() {
				if(counter < list.size()) {
					return Optional.of(list.get(counter++));
				} else {
					return Optional.empty();
				}
			}
			
		};
	}

	@Override
	public SimpleIterator<Integer> random(int size) {
		return new SimpleIterator<Integer>() {
			Random rand = new Random();
			private int counter = 0;
			@Override
			public Optional<Integer> next() {
				if(counter++ < size ) {
					return Optional.of(rand.nextInt(size -1));
				} else {
					return Optional.empty();
				}
			}
			
		};
	}

	@Override
	public SimpleIterator<Integer> quadratic() {
		return new SimpleIterator<Integer>() {
			int current = 1;
			int nOfPrints = current;
			@Override
			public Optional<Integer> next() {
				if(nOfPrints-- > 0) {
					return Optional.of(current);
				} else {
					current ++;
					nOfPrints = current;
					return Optional.of(nOfPrints--);
				}
				 
			}
			
		};
	}

	@Override
	public SimpleIterator<Integer> recurring() {
		return new SimpleIterator<Integer>() {

			@Override
			public Optional<Integer> next() {
				// TODO Auto-generated method stub
				return null;
			}
			
		};
	}

}
