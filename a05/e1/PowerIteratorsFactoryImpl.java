package a05.e1;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.function.UnaryOperator;

public class PowerIteratorsFactoryImpl implements PowerIteratorsFactory {

	@Override
	public PowerIterator<Integer> incremental(int start, UnaryOperator<Integer> successive) {
		
		return new PowerIterator<Integer>() {
			
			List<Integer> list = new LinkedList<>();
			private Integer nextOne = start;
			private int counter = 0;
			@Override
			public Optional<Integer> next() {
				if (counter == 0) {
					counter++;
					list.add(start);
					return Optional.of(start);
				}
				nextOne = successive.apply(nextOne);
				list.add(nextOne);
				return Optional.of(nextOne);
			}

			@Override
			public List<Integer> allSoFar() {
				return list;
			}

			@Override
			public PowerIterator<Integer> reversed() {
				// TODO Auto-generated method stub
				return null;
			}
			
		};
	}

	@Override
	public <X> PowerIterator<X> fromList(List<X> list) {
		return new PowerIterator<X>() {

			List<X> soFar = new LinkedList<>();
			private int counter = 0;
			@Override
			public Optional<X> next() {
				if(counter < list.size()) {
					soFar.add(list.get(counter));
					return Optional.of(soFar.get(counter++));
				}else {
					return Optional.empty();				}
				
			}

			@Override
			public List<X> allSoFar() {
				return soFar;
			}

			@Override
			public PowerIterator<X> reversed() {
				// TODO Auto-generated method stub
				return null;
			}
			
		};
	}

	@Override
	public PowerIterator<Boolean> randomBooleans(int size) {
		return new PowerIterator<Boolean>() {

			List<Boolean> soFar = new LinkedList<>();
			Random rand = new Random();
			private int counter = 0;
			private Boolean nextOne;
			@Override
			public Optional<Boolean> next() {
				if (counter < size) {
					nextOne = rand.nextBoolean();
					soFar.add(nextOne);
					counter++;
					return Optional.of(nextOne);
				} else {
					return Optional.empty();
				}
			}

			@Override
			public List<Boolean> allSoFar() {
				return soFar;
			}

			@Override
			public PowerIterator<Boolean> reversed() {
				// TODO Auto-generated method stub
				return null;
			}
			
		};
	}

}
