package a05.e2;

import javax.swing.*;
import java.util.*;
import java.util.List;
import java.util.Map.Entry;
import java.awt.*;
import java.awt.event.ActionListener;

public class GUI extends JFrame {
    
	private Integer counter = 0;
	private Integer first = 0;
	private JButton latestClicked = new JButton();
	private int size = 0;
	private HashMap<JButton, Pair<Integer, Integer>> buttonMap = new HashMap<>();
	
    private static final long serialVersionUID = -6218820567019985015L;
    private final static int SIZE = 4;
    private final List<JButton> jbs = new ArrayList<>();
    
    public GUI() {
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(100*SIZE, 100*SIZE);
        
        JPanel panel = new JPanel(new GridLayout(SIZE,SIZE));
        this.getContentPane().add(BorderLayout.CENTER,panel);
        
        ActionListener al = (e)->{
            final JButton bt = (JButton)e.getSource();
            hit(bt);
            System.out.println(bt.getX() + " " + bt.getY());
        	if(isOver(bt)) {
        		System.exit(1);
        	}       		
        };
                
        for (int i=0; i<SIZE; i++){
            for (int j=0; j<SIZE; j++){
                final JButton jb = new JButton(" ");
                jb.addActionListener(al);
                panel.add(jb);
                buttonMap.put(jb, new Pair<Integer, Integer>(i, j));
            }
        }
        
        this.setVisible(true);
    }
private void hit(JButton bt) {
		
	    if( areAdj(bt, latestClicked) || first <= 0) {
		bt.setText(counter.toString());
    	counter++;;
    	bt.setEnabled(false);
    	first++;
    	latestClicked = bt;
	}
	
}
	
	private boolean areAdj(JButton bt, JButton button) {
		System.out.println("bt = " + bt.getX() + ", " + bt.getY() + ", latest =" + button.getX() + " , "  + button.getY());
		if((button.getX() - 96) == bt.getX() && button.getY() == bt.getY() ) {
			return true;
		}
		if((button.getX() + 96) == bt.getX() && button.getY() == bt.getY()) {
			return true;
		}
		if((button.getY() - 90) == bt.getY() && button.getX() == bt.getX()) {
			return true;
		}
		if((button.getY() + 90) == bt.getY() && button.getX() == bt.getX()) {
			return true;
		}
		return false;
	}
	
	private boolean isOver(JButton bt) {
		 int disabledButtons = 0;
		 int adjButtons = 0;
   	for(JButton button : buttonMap.keySet()) {
   		if(!button.isEnabled()) {
   			disabledButtons++;
   		}
   		if(areAdj(bt, button) && button.isEnabled()) {
   			adjButtons++;
   		}
   	}
   	System.out.println("ADJ = " + adjButtons);
   	if(disabledButtons == SIZE*SIZE) {
   		return true;
   	}
   	if(adjButtons <= 0) {
   		return true;
   	}
   	return false;
	}
	
	public static void main(String[] args) {
    	new GUI();
    }
}
