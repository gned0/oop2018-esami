package a05.e2;

import javax.swing.JButton;

public interface Logics {
	
	void hit(JButton bt);
	
	boolean areAdj(JButton bt, JButton button);
	
	boolean isOver(JButton bt);
}

