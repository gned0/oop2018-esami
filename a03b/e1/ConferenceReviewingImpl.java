package a03b.e1;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;





public class ConferenceReviewingImpl implements ConferenceReviewing {

	
	private final List<Pair<Integer,Map<Question, Integer>>> reviews = new ArrayList<>();
	Set<Integer> acceptedArticles = new LinkedHashSet<>();
	Set<Pair<Integer,Double>> sortedAcceptedArticles = new LinkedHashSet<>();
	
	@Override
	public void loadReview(int article, Map<Question, Integer> scores) {
		reviews.add(new Pair<>(article, scores));	
		}

	@Override
	public void loadReview(int article, int relevance, int significance, int confidence, int fin) {
		Map<Question, Integer> scores = new HashMap<>();
		scores.put(Question.RELEVANCE, relevance);
		scores.put(Question.SIGNIFICANCE, significance);
		scores.put(Question.CONFIDENCE, confidence);
		scores.put(Question.FINAL, fin);
		reviews.add(new Pair<>(article, scores));


	}

	@Override
	public List<Integer> orderedScores(int article, Question question) {
		List<Integer> ordered = new ArrayList<>();
		for(Pair<Integer,Map<Question, Integer>> coppia : reviews) {
			if(coppia.getX().equals(article)) {
				ordered.add(coppia.getY().get(question));			 
			}
		}
		ordered.sort(new Comparator<Integer>() {

			@Override
			public int compare(Integer o1, Integer o2) {
				return o1-o2;
			}
			
		});
		return ordered;
	}

	@Override
	public double averageFinalScore(int article) {
		Double total = 0.0;
		int counter = 0;
		for(Pair<Integer,Map<Question, Integer>> coppia : reviews) {
			if(coppia.getX().equals(article)) {
				total += coppia.getY().get(Question.FINAL);
				counter++;
			}
		}
		return total/counter;
	}

	@Override
	public Set<Integer> acceptedArticles() {
		
		for(Pair<Integer,Map<Question, Integer>> coppia : reviews) {
			Double total = 0.0;
			int counter = 0;
			for(Integer score : coppia.getY().values()) {
				total += score;
				counter++;
			}
			if(total/counter > 5) {
				if(coppia.getY().get(Question.RELEVANCE) >= 8) {
					acceptedArticles.add(coppia.getX());
					sortedAcceptedArticles.add(new Pair<> (coppia.getX(), total/counter));
				}
			}
		}
		return acceptedArticles; 
	}

	@Override
	public List<Pair<Integer,Double>> sortedAcceptedArticles() {
		
		return this.acceptedArticles().stream()
					.map(e -> new Pair<>(e,this.averageFinalScore(e)))
				   .sorted((e1,e2)->e1.getY().compareTo(e2.getY()))
				   .collect(Collectors.toList());
	}

	@Override
	public Map<Integer, Double> averageWeightedFinalScoreMap() {
		// TODO Auto-generated method stub
		return null;
	}
	
	

}
