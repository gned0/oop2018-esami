package a03b.e2;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javax.swing.*;


public class GUI extends JFrame {
    
	private int limit =5;
	private HashMap<Integer, JButton> buttonMap = new HashMap<>();
	private Boolean goingRight = true;
	private Random rand = new Random();
	private static final long serialVersionUID = -6218820567019985015L;
    private List<JButton> buttons = null; // Java hack to ensure initialisation :(
    private JCheckBox box = null; // Java hack to ensure initialisation :(
    
    public GUI() {
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        
        ActionListener al = (e)->{
            final JButton bt = (JButton)e.getSource();
            	if(!box.isSelected()) {
            		if(buttons.indexOf(bt) < 4) {
            			String tmp = buttons.get((buttons.indexOf(bt)+1)).getText();
                    	buttons.get(buttons.indexOf(bt)+1).setText(bt.getText());
                    	bt.setText(tmp);
            		}
                }
            	if(box.isSelected()) {
            		if(buttons.indexOf(bt) > 0) {
            			String tmp = buttons.get((buttons.indexOf(bt)-1)).getText();
                    	buttons.get(buttons.indexOf(bt)-1).setText(bt.getText());
                    	bt.setText(tmp);
            		}
            	}
            
            else {
            	System.out.println("Limit Reached");
            }
            this.isOver();  
        };
        
        JPanel panel = new JPanel(new FlowLayout());
        this.buttons = IntStream.range(0,limit)
        		                .mapToObj(i->new JButton(Integer.toString(rand.nextInt(9))))
        		                .peek(panel::add)
        		                .peek(jb->jb.addActionListener(al))
        		                .collect(Collectors.toList());
        for(int i = 0; i<limit; i++) {
        	buttonMap.put(i, buttons.get(i));
        }
        this.box = new JCheckBox("Reverse");
        panel.add(this.box);
        this.getContentPane().add(BorderLayout.CENTER,panel);
        this.pack();
        this.setVisible(true);
    }

    private void isOver() {
    	int counter = 0;
    	if(Integer.parseInt(buttonMap.get(0).getText()) <= Integer.parseInt(buttonMap.get(1).getText()) ) {
    		counter++;
    	}
    	if(Integer.parseInt(buttonMap.get(1).getText()) <= Integer.parseInt(buttonMap.get(2).getText()) ) {
    		counter++;
    	}
    	if(Integer.parseInt(buttonMap.get(2).getText()) <= Integer.parseInt(buttonMap.get(3).getText()) ) {
    		counter++;
    	}
    	if(Integer.parseInt(buttonMap.get(3).getText()) <= Integer.parseInt(buttonMap.get(4).getText()) ) {
    		counter++;
    	}
    	System.out.println( counter+1 + " bottoni in ordine crescente");
        if(counter == 4) {
        	System.exit(1);
        }
	}

	public static void main(String[] args) throws java.io.IOException {
        new GUI();
    }
}
